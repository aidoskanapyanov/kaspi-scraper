import csv
from os import name
from selenium import webdriver
from bs4 import BeautifulSoup

# change the path
PATH = '/Users/dima/Downloads/chromedriver' 
LINK = 'https://kaspi.kz/shop/c/categories/?page='
ITEM_PER_PAGE = 12

chrome_options = webdriver.ChromeOptions()
prefs = {"profile.managed_default_content_settings.images": 2}
chrome_options.add_experimental_option("prefs", prefs)
driver = webdriver.Chrome(executable_path=PATH, chrome_options=chrome_options)



def openPage(n):
    driver.get(LINK + n) 
    elem = driver.find_element_by_xpath("//*")
    source_code = elem.get_attribute("outerHTML")
    return BeautifulSoup(source_code, 'lxml')

def toCSV(all_items):
    with open("output.csv","w",newline="") as f: 
        title = all_items[0].keys() 
        cw = csv.DictWriter(f,title,delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        cw.writeheader()
        cw.writerows(all_items)

def readPage(start_page, end_page):
    all_items = []
    for x in range(start_page, end_page):
        names, prices, credit_prices, credit_times  = [], [], [], []

        soup = openPage(str(x))
        
        divTag = soup.find_all("div", {"class": "item-cards-grid__cards"})
        for tag in divTag:
            item_cards = tag.find_all("div", {"class": "item-card"})
            for item_card in item_cards:
                prices.append(item_card.find("span", {"class": "item-card__prices-price"}).text)
                names.append(item_card.find("a", {"class": "item-card__name-link"}).text)
                item_card__instalment = item_card.find("div", {"class": "item-card__instalment"})
                credit_prices.append('0 ₸' if not item_card__instalment else item_card__instalment.find("div", {"class": "item-card__prices-price"}).text)
                credit_times.append('x0' if not item_card__instalment else item_card__instalment.find("div", {"class": "item-card__add-info"}).text)
        
        for i in range(ITEM_PER_PAGE):
            all_items.append({
                'name': names[i], 
                'price': int(prices[i][:-1].replace(' ', '')), 
                'credit_price': int(credit_prices[i][:-1].replace(' ', '')), 
                'credit_duration': int(credit_times[i][1:])
            })
    return all_items



soup = openPage('')
driver.find_element_by_class_name('dialog__close.icon._medium._delete').click()

pg_num = soup.find_all("span", {"class": "filters__count"})
pg_num = int(pg_num[0].text.strip()[1:-1])
pg_num = int(pg_num/ITEM_PER_PAGE) if pg_num%ITEM_PER_PAGE == 0 else int(pg_num/ITEM_PER_PAGE + 1)

# change page offsets
start_page, end_page = 1, pg_num + 1
toCSV(readPage(start_page, end_page))

